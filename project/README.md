Usage:

load GSL modules
> module load gsl

Make all binaries
> make

Gaussian Elimination
> ./Gaussian <filename> <debug>
example:
> ./Gaussian "inputs.dat" true

Cleaning csv data and converting to txt for easy C++ reading. Removes all lines containing -200 and only keeps AH column from AirQualityUCI.csv and outputs AirQualityUCI.txt
> python parse_csv.py

Regression
> ./regression <filename> <degree> <debug>
example:
> ./regression "AirQuality.txt" 5 true

GNUplot 
Plots our data with the calculated polynomial to see fit
> gnuplot data.gnuplot


