!#/usr/bin/env gnuplot

set term png
set output "data.png"
set xtic 0.1
set ytic 0.5
set xrange[0:1]
set yrange[0:2.5]
set xlabel "Time intervals"
set ylabel "Absolute Humidity"


P(x) = 30.3426*x**4 + -54.6458*x**3 + 27.0693*x**2 + -2.59788*x + 0.894306

plot "AirQualityUCI.txt", \
     P(x) 
