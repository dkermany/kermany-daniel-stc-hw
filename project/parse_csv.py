from __future__ import division
import csv

with open("AirQualityUCI.csv") as file:
  readCSV = csv.reader(file, delimiter=";")
  headers = readCSV.next()
  data = []
  for row in readCSV:
    data.append(row)
  AH_header = headers.index("AH")
  AH = []
  for i in range(len(data)):
    for j in range(len(data[i])):
      if j == AH_header and data[i][j] not in ["", "-200"]:
        AH.append(data[i][j])  

  x = []
  for i in range(len(AH)):
    x.append(i/len(AH))

  print("{} Absolute Humidity data points found".format(len(AH)))
  assert(len(AH) == len(x))
  with open("AirQualityUCI.txt", "w+") as txt:
    for i in range(len(AH)):
      txt.write("{}\t{}\n".format(x[i], AH[i]))
      
   
  print("AirQualityUCI.txt Created") 
