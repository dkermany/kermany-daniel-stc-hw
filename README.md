# README #

This is the repo for the howmework assignements for STC Fall 2017 
### Instructions ###
Please see individual folders (HW!, HW2 etc) for specific instructions 

### Need Help? ###

Questions? Don't forget to use our piazza message board!https://piazza.com/utexas/fall2017/sds335394
and also you can contact Anne Bowen adb@tacc.utexas.edu or Damon McDougall dmcdougall@tacc.utexas.edu
